package com.target.spring_boot_maven;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/web")
public class firstclass {
	@GetMapping("/web1")
	public String web1() {
		return "spring boot first page with get mapping";
	}
	@GetMapping("")
	public String web() {
		return "spring boot first page";
	}

}